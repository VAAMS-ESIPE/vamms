package com.projet.app;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.SQLException;

import com.projet.connection.DataSource;

public class ServerAcceptor {

	public static void main(String[] args) throws SQLException, IOException {
		ServerSocket server = null;
		DataSource ds = new DataSource();

		try {
			server = new ServerSocket(2009);
		}
		catch(IOException e){
			throw new IOException ("Port not available " + e.getMessage());
		}

		while(true) {
			Connection con = null; 
			try {
				con = DataSource.getConnectionPool();
				System.out.println("Connection ok");
			} catch (Exception e) {
				throw new SQLException("Any connection received");
			}
			Socket sc = server.accept();
			
			RequestHandler rh = new RequestHandler(sc, con);
			Thread th = new Thread(rh);
			th.start();
			System.out.println("RequestHandler lancé");
		}
	}
}
