package com.projet.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

public class JDBCConnectionPool {

	private ArrayList<Connection> availableConnections;
	private ArrayList<Connection> unavailableConnections;
	private static int MAX_CONNECTIONS;
	private static int MIN_CONNECTIONS;

	public JDBCConnectionPool() throws SQLException {
		this.availableConnections = new ArrayList<>();
		this.unavailableConnections = new ArrayList<>();
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		addInitialConnection();
	}


	public void addInitialConnection() throws SQLException {
		try {
			for(int i=0; i<10; i++) {
				availableConnections.add(DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5434/PDS","postgres", "ghd" ));
			}
		} catch (SQLException e) {
			throw new SQLException("Any connection");
		}
	}

	public synchronized Connection getConnectionPool() throws SQLException {
		if(availableConnections.isEmpty()) {
			if(MAX_CONNECTIONS > unavailableConnections.size()) {
				availableConnections.add(DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5434/PDS", "postgres", "ghd"));
			}
			else {
				throw new SQLException("le quotat a ete atteint");
			}
		}
		Connection con = availableConnections.remove(availableConnections.size()-1);
		unavailableConnections.add(con);
		return con;
	}

	public synchronized boolean releaseConnection(Connection con) throws SQLException {
		boolean answer = unavailableConnections.remove(con);
		availableConnections.add(con);
		return answer;
	}

	public void closeAllConnection(){
		for (int i=0; i < availableConnections.size(); i++){
			try {
				availableConnections.get(i).close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			availableConnections.remove(i);
		}
		for (int i=0; i < unavailableConnections.size(); i++){
			try {
				unavailableConnections.get(i).close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			unavailableConnections.remove(i);
		}
		System.out.println("All the connections are closed.");
	}


	public int getSize() {
		// TODO Auto-generated method stub
		int size = availableConnections.size()+ unavailableConnections.size();
		return size;
	}

}
