package com.projet.connection;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AccessConnection {
	private static AccessConnection accessCon = new AccessConnection();
	private static  String  DRIVER;
	private static  String URL ;
	private static  String USERNAME ;
	private static  String PASSWORD ;
	private static int NBR_INITIAL;
	private static int NBR_MAX;
	private static int PORT;

	private AccessConnection(){
		Properties p = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("./ressources/server.properties");
			p.load(input);		


			DRIVER = p.getProperty("DRIVER");
			URL = p.getProperty("URL");
			USERNAME = p.getProperty("USERNAME");
			PASSWORD = p.getProperty("PASSWORD");
			NBR_INITIAL = Integer.parseInt(p.getProperty("NBR_INITIAL"));
			NBR_MAX = Integer.parseInt(p.getProperty("NBR_MAX"));
			PORT =  Integer.parseInt(p.getProperty("PORT"));


		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static AccessConnection getAccessCon() {
		return accessCon;
	}

	public static void setAccessCon(AccessConnection accessCon) {
		AccessConnection.accessCon = accessCon;
	}

	public static String getDRIVER() {
		return DRIVER;
	}

	public static void setDRIVER(String dRIVER) {
		DRIVER = dRIVER;
	}

	public static String getURL() {
		return URL;
	}

	public static void setURL(String uRL) {
		URL = uRL;
	}

	public static String getUSERNAME() {
		return USERNAME;
	}

	public static void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}

	public static String getPASSWORD() {
		return PASSWORD;
	}

	public static void setPASSWORD(String pASSWORD) {
		PASSWORD = pASSWORD;
	}

	public static int getNBR_INITIAL() {
		return NBR_INITIAL;
	}

	public static void setNBR_INITIAL(int nBR_INITIAL) {
		NBR_INITIAL = nBR_INITIAL;
	}

	public static int getNBR_MAX() {
		return NBR_MAX;
	}

	public static void setNBR_MAX(int nBR_MAX) {
		NBR_MAX = nBR_MAX;
	}

	public static int getPORT() {
		return PORT;
	}

	public static void setPORT(int pORT) {
		PORT = pORT;
	}

	@Override
	public String toString() {
		return "AccessConnection [getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
}
