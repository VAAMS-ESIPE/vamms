package com.projet.connection;

import java.sql.Connection;
import java.sql.SQLException;

public class DataSource {

	private static JDBCConnectionPool jdbcPool;

	public DataSource() throws SQLException {
		jdbcPool = new JDBCConnectionPool();
	}

	public static Connection getConnectionPool() throws SQLException {
		return jdbcPool.getConnectionPool();
	}

	public synchronized boolean releaseConnection(Connection con) throws SQLException {
		return jdbcPool.releaseConnection(con);
	}

	public void closeAllConnection(){
		jdbcPool.closeAllConnection();
	}

	public static int getSize() {
		return jdbcPool.getSize();
	}
}
