package com.database;

public class DAOException extends RuntimeException {
	/**
	*
	* @author Sofia
	*/	
	public DAOException(String message) {
		super(message);
	}
	
	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public DAOException(Throwable cause) {
		super(cause);
	}
	

}
