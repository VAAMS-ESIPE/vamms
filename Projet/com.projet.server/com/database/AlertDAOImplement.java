package com.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.projet.monitoring.Sensor;
import com.projet.monitoring.SensorType;

public class AlertDAOImplement {

	
	private Connection connection;
	private Statement ordre;


	public AlertDAOImplement(Connection co) {
		this.connection=co;
	}


	public void insert(Sensor sensor) throws DAOException {
		SensorType type = sensor.getType();
		int id = sensor.getId();
		String name = sensor.getName();
		
		try {
			ordre = connection.createStatement();
		} catch (SQLException ex) {
			Logger.getLogger(AlertDAOImplement.class.getName()).log(Level.SEVERE, null, ex);
		}

		String all="insert into historique_alertes(id, name, type) values ('"+id+"', '"+name+"', '"+type+"')";

		try {
			ordre.executeUpdate(all);
		} catch (SQLException ex) {
			Logger.getLogger(AlertDAOImplement.class.getName()).log(Level.SEVERE, null, ex);
		}
	}


}
