package com.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;

/**
*
* @author Sofia
*/

public class ConnectionPool {
	
	
	private final static ArrayList<Connection> pool = new ArrayList<Connection>();
	
	
	public ConnectionPool() {
		int nbConnection=10;
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		
		for (int i = 0 ; i < nbConnection ; i++) {
			try {
				Connection co = DriverManager.getConnection("jdbc:postgresql://localhost/PDSVamms", "postgres", "ahaedh94");
				System.out.println("Connexion succès");
				pool.add(co);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void closeAllConnection(){
		for (int i=0; i < pool.size(); i++){
			try {
				pool.get(i).close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			pool.remove(i);
		}
		System.out.println("Toutes les connexions sont fermées.");
	}
	

	public synchronized static Connection getConnectionPool() {
		System.out.println(pool.size()-1);
		if(pool.isEmpty()) {
			System.out.println("Il n'y a pas de connexion disponible");
			return null;
		}
		return pool.remove(pool.size()-1);
	}
	
	
	public synchronized static void ConnectionToPool(Connection co) {
		pool.add(co);
		System.out.println(pool.size());
	}
       
	
       
}


