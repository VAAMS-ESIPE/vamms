package com.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.projet.model.Sensor;

public class SensorDAOImplement implements SensorDAO{
	/**
	*
	* @author Sofia
	*/
	
	
	private Connection connection;
	private Statement ordre;


	public SensorDAOImplement(Connection co) {
		this.connection=co;
	}


	public void insert(Sensor sensor) throws DAOException {
		String type = sensor.getType();
		String brand = sensor.getBrand();
		double price = sensor.getPrice();
		//int quantity = sensor.getNumber();
		int capacity = sensor.getCapacity();
		int duration = sensor.getOperating_time();
		String loc = sensor.getLocalisation();
		double maint_price = sensor.getAnual_maintenance_price();

		try {
			ordre = connection.createStatement();
		} catch (SQLException ex) {
			Logger.getLogger(SensorDAOImplement.class.getName()).log(Level.SEVERE, null, ex);
		}

		String all="insert into capteurs(nom_capteur, marque, prix, portee, duree_garantie, localisation, cout_maintenance_annee) values ('"+type+"', '"+brand+"', '"+price+"', '"+capacity+"', '"+duration+"', '"+loc+"', '"+maint_price+"')";

		try {
			ordre.executeUpdate(all);
		} catch (SQLException ex) {
			Logger.getLogger(SensorDAOImplement.class.getName()).log(Level.SEVERE, null, ex);
		}
	}


	public void delete(String type, String brand, String loc) throws DAOException {

		try {
			ordre = connection.createStatement();
		} catch (SQLException ex) {
			Logger.getLogger(SensorDAOImplement.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		System.out.println("Avant");
		String all="delete from capteurs where nom_capteur='"+type+"' and marque='"+brand+"' and localisation='"+loc+"'";

		try {
			ordre.executeUpdate(all);
		} catch (SQLException ex) {
			Logger.getLogger(SensorDAOImplement.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		System.out.println("Après");


	}

	/*public void update(Sensor sensor) throws DAOException {

	}*/

	public void find(String type, String brand) throws DAOException {
		/*try {
			ordre = connection.createStatement();
		} catch (SQLException ex) {
			Logger.getLogger(SensorDAOImplement.class.getName()).log(Level.SEVERE, null, ex);
		}*/

		String all = "Select localisation from capteurs where nom_capteur='"+type+"' and marque='"+brand+"'";
		String loc ="";
		try {
			PreparedStatement ps = null;
			ps = connection.prepareStatement(all);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				System.out.println(rs.getString("localisation"));
				loc += rs.getString("localisation")+ "\n";
			}
		} catch (SQLException ex) {
			Logger.getLogger(SensorDAOImplement.class.getName()).log(Level.SEVERE, null, ex);
		}

	}





}
