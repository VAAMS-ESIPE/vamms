package com.database;
import com.projet.model.Sensor;

/**
*
* @author Sofia
*/

public interface SensorDAO {
	
	void insert(Sensor sensor) throws DAOException;
	void delete(String type, String brand, String loc) throws DAOException;
	//void update(Sensor sensor) throws DAOException;
	void find(String type, String brand) throws DAOException;

}
