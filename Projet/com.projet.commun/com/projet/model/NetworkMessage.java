package com.projet.model;

import java.io.Serializable;

import com.projet.monitoring.SensorMessage;

public class NetworkMessage implements Serializable {
	public final User userInfo;
	public final SensorMessage sensorMessage;

	@Override
	public String toString() {
		return "NetworkMessage{" +
				"sensorMessage=" + sensorMessage +
				'}';
	}
	public NetworkMessage(SensorMessage sensorMessage) {
		this.sensorMessage = sensorMessage;
		this.userInfo = null;
	}

	public NetworkMessage(User userInfo) {
		this.sensorMessage = null;
		this.userInfo = userInfo;
	}

	public NetworkMessage(SensorMessage sensorMessage, User userInfo) {
		this.sensorMessage = sensorMessage;
		this.userInfo = userInfo;
	}
}
