package com.projet.model;

public enum AlertType {

	FIRE (10, "Fire"), 
	INTRUSION (3, "Intrusion"),
	LIGHT (5, "Light"),
	TEMPERATURE (2, "Temperature"),
	PRESENCE (3, "Presence"),
	PASSAGE (1, "Passage");
	
	
	public final int treshold;
	public final String name;
	
	AlertType(int treshold, String name) {
		this.treshold= treshold;
		this.name= name;
	}
	
	
}

