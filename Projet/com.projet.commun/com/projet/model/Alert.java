package com.projet.model;

import java.io.Serializable;

public class Alert implements Serializable {
	public final AlertType id;
	private int occurence = 1;


	public Alert(AlertType id) {
		this.id = id;
	}
	
	public Alert(AlertType id, int occ) {
		this.id = id;
		this.occurence = occ;
	}

	public boolean SameType(Alert alert) {
		return id == alert.id;
	}

	public boolean addOccurence(Alert alert) throws IllegalArgumentException {
		if(!SameType(alert)) {
			throw new IllegalArgumentException("The alerts are not compatible");
		}
		occurence += alert.occurence;
		return occurence >= id.treshold;
		
	}

	public int getOccurence() {
		return occurence;
	}

	public void setOccurence(int occurence) {
		this.occurence = occurence;
	}

	public AlertType getId() {
		return id;
	}
	
	



}
