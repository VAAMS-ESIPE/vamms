package com.projet.monitoring;

import java.util.ArrayList;

public class SensorList {
	public ArrayList<SensorLine> listSensors;

	public SensorList() {
		super();
		this.listSensors = new ArrayList<SensorLine>();
	}

	public String toString() {
		if(this.listSensors.isEmpty()) {
			return "Any sensors";
		}
		String result = "";
		for (SensorLine sensorLine : listSensors)
		{
			result += sensorLine+"\n";
		}
		return result;
	}
	


	public void addSensor(Sensor s) {
		SensorLine sl = new SensorLine(s);
		this.listSensors.add(sl);
	}

	public void deleteArc(Sensor s)
	{
		for (SensorLine sl : listSensors)
		{
			if(s == sl.getSensor())
			{
				this.listSensors.remove(sl);
				return;
			}
		}
	}

	public void clearSensorsLines() {
		this.listSensors.clear();
	}

	public static void main(String[] args) {
		System.out.println("Creation de 3 capteurs");
		Sensor s2 = new Sensor(2, "s2", SensorType.INTRUSION);		
		Sensor s3 = new Sensor(3, "s3", SensorType.LIGHT);		
		Sensor s4 = new Sensor(4, "s4", SensorType.PASSAGE);
		System.out.println("Sensor 2 : "+ s2);
		System.out.println("Sensor 3 : "+ s3);
		System.out.println("Sensor 4 : "+ s4);
		System.out.println();
		System.out.println("2 : création d'une liste");
		SensorList listS = new SensorList();
		System.out.println(listS);
		System.out.println("3 : ajout des capteurs dans la liste");
		listS.addSensor(s2);
		listS.addSensor(s3);
		listS.addSensor(s4);
		
	}
}
