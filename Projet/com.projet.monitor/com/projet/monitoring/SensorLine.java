package com.projet.monitoring;

public class SensorLine {
	
	private Sensor sensor;
	
	public SensorLine(Sensor sensor) {
		super();
		this.sensor = sensor;
	}
	
	public Sensor getSensor() {
		return sensor;
	}

	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}



	@Override
	public String toString() {
		return "SensorLine [sensor=" + sensor + "]";
	}
	
	public int getSensorId()
	{
		int index = new Integer(sensor.getId());
		return index;
	}

	
	
	public static void main (String[] args) {
		Sensor s1 = new Sensor(1, "s1", SensorType.FIRE);
		SensorLine test = new SensorLine(s1);
		System.out.println(test);	
		System.out.println("nom de s1"+s1.getName());
	}
}

