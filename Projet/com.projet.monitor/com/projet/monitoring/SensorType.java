package com.projet.monitoring;

public enum SensorType {

	FIRE ("Fire"), 
	INTRUSION ("Intrusion"),
	LIGHT ("Light"),
	TEMPERATURE ("Temperature"),
	PRESENCE ("Presence"),
	PASSAGE ("Passage");


	private String name;

	SensorType(String name) {
		this.name = name;

	}

	public static SensorType getSensorType(String sensorType) {
		SensorType[] values = SensorType.values();
		for(SensorType value : values) {
			if(value.toString().equalsIgnoreCase(sensorType))
				return value;
		}
		return null;
	}


	public String getName() {
		return name;
	}


}
