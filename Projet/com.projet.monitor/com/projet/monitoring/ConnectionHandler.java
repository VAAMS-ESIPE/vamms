package com.projet.monitoring;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.function.Function;

import com.projet.model.NetworkMessage;

public class ConnectionHandler {
	 private ArrayList<Socket> clients = new ArrayList<>();
	    private ServerSocket serverSocket;
	    private Function<NetworkMessage, Void> dispatcher;

	    public ConnectionHandler(Function<NetworkMessage, Void> dispatcher, int port) throws IOException {
	        this.dispatcher = dispatcher;
	        serverSocket = new ServerSocket(port);
	    }

	    private NetworkMessage getMsgFrom(Socket client) {
	        try {
	            ObjectInputStream in = new ObjectInputStream(client.getInputStream());
	            return (NetworkMessage)in.readObject();
	        } catch (Exception e) {}

	        return null;
	    }

	    private boolean sendMsgTo(Socket client, NetworkMessage msg) {
	        try {
	            ObjectOutputStream out = new ObjectOutputStream(client.getOutputStream());
	            out.writeObject(msg);
	        } catch (Exception e) { e.printStackTrace(); return false; }

	        return true;
	    }

	    public void processClients() {
	        for (int i = clients.size() - 1; i >= 0; i--) {
	            NetworkMessage msg = getMsgFrom(clients.get(i));
	            if (msg == null) {
	                clients.remove(i);
	                continue;
	            }

	            dispatcher.apply(msg);
	        }
	    }

	    public void broadcast(NetworkMessage msg) {
	        for (int i = clients.size() - 1; i >= 0; i--) {
	            if (!sendMsgTo(clients.get(i), msg)) {
	                clients.remove(i);
	            }
	        }
	    }

	    public void accept() {
	        try {
	            while (true) {
	                Socket client = serverSocket.accept();
	/*
	                // Il faut verifier la connection
	                NetworkMessage msg = getMsgFrom(client);

	                if (msg == null || msg.userInfo == null)
	                    continue;

	                // Are the informations correct ?
	                if (!conPool.isCorrectUser(msg.userInfo.username, msg.userInfo.passordHash))
	                    continue;
	*/

	                // Le client est correcte on l'ajoute
	                clients.add(client);
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }

}
