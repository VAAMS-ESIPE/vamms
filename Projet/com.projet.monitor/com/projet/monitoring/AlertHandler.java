package com.projet.monitoring;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Function;

import com.projet.model.Alert;

public class AlertHandler {

	private SensorMessage currentAlert = null; //faire une liste 
	private HashMap<String, Alert> mapAlerts;
	private ArrayList<Function<SensorMessage, Void>> dispatchFcts = new ArrayList<>();


	public AlertHandler() {
		this.mapAlerts =  new HashMap<>();
	}

	public void Process(SensorMessage sensorMessage) {
		//le capteur est défectueux
		if (sensorMessage == null) { 
			return;
		}

		//le capteur est "normal"
		if(sensorMessage.alert == null) {
			//on retire la dernière alerte
			mapAlerts.remove(sensorMessage.idSensor);
			return;
		}

		//on récupère la derniere alerte envoyée dans la map
		Alert lastAlert = mapAlerts.get(sensorMessage.idSensor);


		if(lastAlert == null) {
			lastAlert = new Alert(sensorMessage.alert.id, 0);
			mapAlerts.put(sensorMessage.idSensor, sensorMessage.alert);
		}

		try {
			if(!lastAlert.addOccurence(sensorMessage.alert)) {
				return;
			}

			Dispatch(sensorMessage);
			return;
		}
		catch(IllegalArgumentException e){
			mapAlerts.put(sensorMessage.idSensor, sensorMessage.alert);
		}

	}

	public void addAlertEvent(Function<SensorMessage, Void> event) {
		this.dispatchFcts.add(event);
	}

	private void Dispatch(SensorMessage sensorMessage) {
		//ça dispache les erreurs vers l'extérieur (bdd, client ihm)
		System.err.println("{AlertHandler}[Dispatch]: " + sensorMessage.toString());

		for (Function<SensorMessage, Void> c : this.dispatchFcts) {
			c.apply(sensorMessage);
		}
		//add le currentAlert dans la liste
		currentAlert = sensorMessage;
	}


	public boolean StopCurrentAlert() {
		if(currentAlert == null) { //if currentAlert.size()==null 
			return false;
		}
		mapAlerts.remove(currentAlert.idSensor);
		currentAlert = null;
		return true; 
		/* modifier le stopAlert, 
		 * supprime les alertes de la liste
		 */
	}
}
