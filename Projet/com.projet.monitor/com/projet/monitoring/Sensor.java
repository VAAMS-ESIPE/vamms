package com.projet.monitoring;

public class Sensor {
	private int id;
	private String name;
	private SensorType type;
	
	public Sensor(int id, String name, SensorType type) {
		this.id = id;
		this.name = name;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SensorType getType() {
		return type;
	}

	public void setType(SensorType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return " [id=" + id + ", name=" + name + ", type=" + type + "]";
	}

	
}
