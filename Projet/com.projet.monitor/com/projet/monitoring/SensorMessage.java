package com.projet.monitoring;

import java.io.Serializable;

import com.projet.model.Alert;

public class SensorMessage implements Serializable {


	public final String idSensor;
	public final Alert alert;

	public SensorMessage(String idSensor, Alert alert) {
		this.idSensor = idSensor;
		this.alert = alert;
	}
	
	
	@Override
	public String toString() {
		return "SensorMessage [idSensor=" + idSensor + ", alert=" + alert + "]";
	}

	
}
