package com.projet.view;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.TimerTask;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.projet.model.Alert;
import com.projet.model.AlertType;
import com.projet.monitoring.AlertHandler;
import com.projet.monitoring.Sensor;
import com.projet.monitoring.SensorLine;
import com.projet.monitoring.SensorList;
import com.projet.monitoring.SensorMessage;
import com.projet.monitoring.SensorType;

import co.projet.app.MainIHM;

public class IHMMonitoring extends JFrame {

	private JPanel contentPane;
	private JTextArea textArea;
	SensorList sl = new SensorList();
	private DefaultListModel <Sensor> dlmSensors = new DefaultListModel<>();
	private JList<Sensor> sensorList = new JList<>(dlmSensors);
	

	private String sensorID = "[IHM_MONITOR_FAKE_SENSOR]";

	private AlertHandler alertHandler;

	private final SimpleDateFormat sdf = new SimpleDateFormat("[HH:mm:ss]");

	public void Log(String msg){
		if(Calendar.getInstance() == null) {
			System.out.println("Error");
		}
		this.textArea.append(sdf.format(Calendar.getInstance().getTime()) +
				" : " + msg + System.lineSeparator());
	}
	private SensorMessage buildSensorMsg(AlertType alertType) {
		scheduleTask();
		return new SensorMessage(sensorID, new Alert(alertType));
	}
	

	private void scheduleTask() {
		// TODO Auto-generated method stub
		MainIHM.resetTimer();

		MainIHM.timer.schedule(new TimerTask() {
			@Override
			public void run() {
				alertHandler.Process(new SensorMessage(sensorID, null));
				Log("Normal sensor state");
			}}, MainIHM.freqSendNormal, MainIHM.freqSendNormal);
	}

	/**
	 * Create the frame.
	 */
	public IHMMonitoring(AlertHandler alertHandler) {
		this.alertHandler = alertHandler;

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 801, 601);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(5, 5, 758, 219);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 752, 213);
		panel_1.add(scrollPane);

		textArea = new JTextArea();
		textArea.setEditable(false);
		scrollPane.setViewportView(textArea);
		textArea.setLineWrap(true);
		textArea.setBackground(Color.WHITE);

		JPanel panel = new JPanel();
		panel.setBounds(43, 236, 205, 337);
		contentPane.add(panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));


		ArrayList<JButton> alert_btns = new ArrayList<>();
		for (AlertType alertType : AlertType.values()) {
			JButton btn = new JButton(alertType.name);
			panel.add(btn);
			btn.addActionListener(e -> alertHandler.Process(buildSensorMsg(alertType)));

			alert_btns.add(btn);
		}

		JButton btnStop = new JButton("STOP");
		btnStop.setBackground(Color.RED);
		panel.add(btnStop);
		btnStop.addActionListener(e -> {
			if (alertHandler.StopCurrentAlert()) {
				Log("Alert stopped!");
			}
			else Log("No alert yet (fortunately)");
		});
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(351, 281, 395, 258);
		contentPane.add(panel_2);
		panel_2.setLayout(null);

		JScrollPane scrollPane_1 = new JScrollPane(sensorList);
		scrollPane_1.setBounds(6, 6, 383, 246);
		panel_2.add(scrollPane_1);


		ListSelectionListener listListener = listSelectionEvent -> {
			Sensor selectedSensor = sensorList.getSelectedValue();
			sensorID = selectedSensor.getName();
			for (int i = 0; i < alert_btns.size(); i++) {
			    alert_btns.get(i).setEnabled(i == selectedSensor.getType().ordinal());
			}
		};
		sensorList.addListSelectionListener(listListener);

		scheduleTask();
		DataSensor();
	}

	private Random random = new Random();
	private Sensor generateRndSensor(int idx) {
		SensorType sensorType = SensorType.values()[random.nextInt(SensorType.values().length)];
		return new Sensor(idx, "SENSOR_" + idx + "_" + sensorType.getName(), sensorType);
	}
	
	public void DataSensor() {
		final int nbFakeSensor = 20;

		for (int i = 0; i < nbFakeSensor; i++)
			sl.addSensor(generateRndSensor(i));

		for (SensorLine sens : sl.listSensors) {
			this.dlmSensors.addElement(sens.getSensor());
		}
	}

}
