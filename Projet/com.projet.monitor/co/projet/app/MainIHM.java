package co.projet.app;

import java.awt.EventQueue;
import java.io.IOException;
import java.util.Timer;

import com.projet.model.AlertType;
import com.projet.model.NetworkMessage;
import com.projet.monitoring.AlertHandler;
import com.projet.monitoring.ConnectionHandler;
import com.projet.view.IHMMonitoring;

public class MainIHM {

	public static final long freqSendNormal = 2000;
	public static Timer timer = new Timer();
	public static final AlertHandler alertHandler = new AlertHandler();


	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		ConnectionHandler connectionHandler = new ConnectionHandler(MainIHM::msgHandler, 2022);

		alertHandler.addAlertEvent(sensorMessage -> {
			System.err.println("BROADCAST");
			connectionHandler.broadcast(new NetworkMessage(sensorMessage));
			return null;
		});

		EventQueue.invokeLater(() -> {
			try {
				IHMMonitoring frame = new IHMMonitoring(alertHandler);

				alertHandler.addAlertEvent(sensorMessage -> {
					frame.Log("[ALERT] : " + sensorMessage.toString());
					if (sensorMessage.alert.id == AlertType.FIRE)
						frame.Log("    [ACTION] : Calling firefighters..." );
					return null;
				});

				frame.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		(new Thread(() -> connectionHandler.accept())).start();

		(new Thread(() -> {
			try {
				while (true) {
					connectionHandler.processClients();
					Thread.sleep(100);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		})).start();
		

	}

	private static Void msgHandler(NetworkMessage msg){
		alertHandler.Process(msg.sensorMessage);

		return null;
	}

	public static void resetTimer() {
		// TODO Auto-generated method stub
		MainIHM.timer.cancel();
		MainIHM.timer.purge();

		MainIHM.timer = new Timer();

	}

}
