package com.projet.map;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

public class Map extends JPanel  implements MouseListener {
   private static final int RECT_X = 500;
   private static final int RECT_Y = RECT_X;
   private static final int RECT_WIDTH = 300;
   private static final int RECT_HEIGHT = RECT_WIDTH;
  
   public Map() {
		addMouseListener(this);
				
	}
   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      // draw the rectangle here
      
      
      g.drawRect(RECT_X, RECT_Y, RECT_WIDTH, RECT_HEIGHT);
      g.drawRect(1000,500, 300, 300);
      g.drawRect(500,50,300,300);
      g.drawRect(1000,50, 300, 300);
      g.drawRect(1000,50, 200, 100);
      g.drawLine(1000, 50, 500, 50);
      g.drawLine(1000, 800, 500, 800);
      g.drawLine(500, 500, 500, 300);
      g.drawLine(1300, 500, 1300, 300);
      g.drawArc(450,335,200,180,120,120); 
      
      
      // Logo smoke sensors
   
	try {
		BufferedImage img = ImageIO.read(new File("ressources/LogoMap/Fumee.jpg"));
		
		g.drawImage(img, 515, 60, 50, 50, this);
		g.drawImage(img, 1015, 60, 50, 50, this);
		g.drawImage(img, 870, 400, 50, 50, this);
		g.drawImage(img, 475, 400, 25, 25, this);
		
		
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
      //Logo motion sensors
	try {
		BufferedImage img = ImageIO.read(new File("ressources/LogoMap/Mouvement.jpg"));
		g.drawImage(img, 515, 400, 50, 50, this);
		g.drawImage(img, 1230, 400, 50, 50, this);
		g.drawImage(img, 870, 60, 50, 50, this);
		g.drawImage(img, 870, 735, 50, 50, this);
		g.drawImage(img, 515, 515, 50, 50, this);
		g.drawImage(img, 1230, 285, 50, 50, this);
	
	} catch (Exception e) {
		// TODO: handle exception
	}
	
	//Logo temperature sensors
	try {
		BufferedImage img = ImageIO.read(new File("ressources/LogoMap/Temperature.jpg"));
		g.drawImage(img, 735, 285, 50, 50, this);
		g.drawImage(img, 1230, 735, 50, 50, this);
			
	} catch (Exception e) {
		// TODO: handle exception
	}
     //Logo brightness sensors
	try {
		BufferedImage img = ImageIO.read(new File("ressources/LogoMap/Luminosit�.jpg"));
		g.drawImage(img, 735, 735, 50, 50, this);
		g.drawImage(img, 1015, 515, 50, 50, this);
	} catch (Exception e) {
		// TODO: handle exception
	}
	
	// Room name
	String ac ="ANIMATION COLLECTIVE";
	g.drawString(ac,575, 650);
	
	String sa = "SALLE ACTIVITE";
	g.drawString(sa,1100, 650);
	
	String a = "ACCUEIL";
	g.drawString(a,1120, 225);
	
	String sds ="SALLE DE SOINS";
	g.drawString(sds,600, 225);
	
	String co ="CENTRE OPERATIONNEL";
	g.drawString(co,1020, 135);
	
	
	// Sensor status
	g.setColor(Color.GREEN);
	g.fillOval(580, 70, 20, 20);
	g.fillOval(700,300, 20, 20);
	g.fillOval(580, 415, 20, 20);
	g.fillOval(580, 530, 20, 20);
	g.fillOval(1075, 70, 20, 20);
	g.fillOval(1075, 530, 20, 20);
	g.fillOval(1200,300, 20, 20);
	g.fillOval(700,750, 20, 20);
	g.fillOval(1200,750, 20, 20);
	g.fillOval(885,705, 20, 20);
	g.fillOval(885,115, 20, 20);
	g.fillOval(885,460, 20, 20);
	g.fillOval(1200,415, 20, 20);
	g.fillOval(475,430, 20, 20);
   }
   
   // Location mouseClicked
   private boolean location(Point mouse, int x,int y, int w,int h) {
	   Rectangle rec = new Rectangle(x,y,w,h);
	 
	   if(rec.contains(mouse))
		   return true;
	   else
		   return false ;
   }
   
   

   @Override
   public Dimension getPreferredSize() {
      // so that our GUI is big enough
      return new Dimension(RECT_WIDTH + 2 * RECT_X, RECT_HEIGHT + 2 * RECT_Y);
   }

   // create the GUI 
   private static void createAndShowGui() {
      Map mainPanel = new Map();
      JFrame frame = new JFrame("Map");
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.getContentPane().add(mainPanel);
      frame.pack();
      frame.setLocationByPlatform(true);
      frame.setVisible(true);
  }
   
 	

		@Override
		public void mouseClicked(MouseEvent e) {
		Point mouse = e.getPoint();
		if(location(mouse,1000,500,300,300)) {
			JOptionPane.showMessageDialog(null,"id : 1 "
					+ "type : smoke "
					+ " status : normal "
					+ " ip : 192.168.1.1 "
					+ " mac : 00:1B:44:11:3A:B7 ","Info capteur"
							+ " type : ",JOptionPane.INFORMATION_MESSAGE);
		}
		if(location(mouse,500,50,300,300)) {
			JOptionPane.showMessageDialog(null,"La saisie est vide","Info capteurs",JOptionPane.ERROR_MESSAGE);
		}
			
		if(location(mouse,1000,50, 300, 300)) {
			JOptionPane.showMessageDialog(null,"La saisie est vide","Info capteurs",JOptionPane.ERROR_MESSAGE);
		}
		
		if(location(mouse,1000,50, 200, 100)) {
			JOptionPane.showMessageDialog(null,"La saisie est vide","Info capteurs",JOptionPane.ERROR_MESSAGE);
		}
		
		if(location(mouse,RECT_X, RECT_Y, RECT_WIDTH, RECT_HEIGHT)) {
			JOptionPane.showMessageDialog(null,"La saisie est vide","Info capteurs",JOptionPane.ERROR_MESSAGE);
		}
		
		
		
		
 }  

			
			
	

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
   
   
		
		
		
		
   public static void main(String[] args) {
      SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            createAndShowGui();
           
           
         }
      });
	  
   }
}