package com.projet.app;


import java.awt.EventQueue;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

import com.projet.model.NetworkMessage;
import com.projet.view.AddSensor_ihm;
import com.projet.view.IHM;

public class Main {
	public static IHM frame;
	private static Socket socket;

	public static boolean connectServer(String host, int port) {
		try {
			socket = new Socket(host, port);
		} catch (IOException e) {
			return false;
		}

		return socket.isConnected();
	}

	public static NetworkMessage getMsgFromServer() throws Exception {
		if (socket == null)
			return null;

		ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
		return (NetworkMessage) in.readObject();
	}


	public static void handleServerCommunication() {
		new Thread(() -> {
			try {
				while (true) {
					NetworkMessage msg = getMsgFromServer();
					if (msg == null)
						continue;

					frame.Log(msg.toString());
				}
			}
			catch (Exception e) { e.printStackTrace(); }

		}).start();
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			try {
				frame = new IHM();
				frame.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
}
