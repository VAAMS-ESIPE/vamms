package com.projet.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import com.projet.app.Main;

public class IHM extends JFrame{
	private JPanel contentPane;
	private JTextArea textArea;


	private final SimpleDateFormat sdf = new SimpleDateFormat("[HH:mm:ss]");

	public void Log(String msg){
		this.textArea.append(sdf.format(Calendar.getInstance().getTime()) +
				" : " + msg + System.lineSeparator());
	}

	protected void paintComponent(Graphics g) {

		super.paintComponents(g);

		try {
			BufferedImage img = ImageIO.read(new File("ressources/LohoVAMMS.png"));
			g.drawImage(img, 50, 20, 100, 100, this);

		}catch(IOException e) {

		}
	}

	public IHM() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(75, 159, 455, 226);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(30, 6, 386, 282);
		panel_1.add(scrollPane);

		textArea = new JTextArea();
		textArea.setEditable(false);
		scrollPane.setViewportView(textArea);
		textArea.setLineWrap(true);
		textArea.setBackground(Color.WHITE);

		JButton btnConnect = new JButton("Connect");
		btnConnect.setBounds(238, 118, 96, 29);
		contentPane.add(btnConnect);
		btnConnect.addActionListener(e -> {
			String host = "localhost";
			int port = 2022;

			if (!Main.connectServer(host, port)) {
				Log("unable to connect to '" + host + ":" + port + "'");
				return;
			}

			btnConnect.setEnabled(false);

			Log("successfully connected to '" + host + ":" + port + "' !");
			Main.handleServerCommunication();
		});

	}


}
