package com.projet.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class PLogin extends JPanel  {

	private JTextField email;
	private JPasswordField mdp;
	private JButton jLogin;

	public PLogin() {
		super();
		this.setLayout(new GridBagLayout());
		JPanel form = new JPanel(new GridLayout(5, 1));
		form.add(new JLabel("Email"));
		mdp = new JPasswordField();
		email = new JTextField();
		jLogin = new JButton("LOGIN");
		form.add(email);
		form.add(new JLabel("Password"));
		form.add(mdp);
		form.add(jLogin);
		form.setPreferredSize(new Dimension(500,500));
		this.add(form);
		form.setBackground(Color.WHITE);

	}


	public void paintComponent(Graphics g){
		try {
			Image img = ImageIO.read(new File(""));
			//Pour une image de fond
			g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} catch (IOException e) {
			e.printStackTrace();
		}                
	}               

	

	public JTextField getEmail() {
		return email;
	}

	public void setEmail(JTextField email) {
		this.email = email;
	}

	public JPasswordField getMdp() {
		return mdp;
	}

	public void setMdp(JPasswordField mdp) {
		this.mdp = mdp;
	}

	public JButton getjLogin() {
		return jLogin;
	}

	public void setjLogin(JButton jLogin) {
		this.jLogin = jLogin;
	}

}
