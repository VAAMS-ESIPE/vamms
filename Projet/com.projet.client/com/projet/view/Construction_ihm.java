package com.projet.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;

public class Construction_ihm extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Construction_ihm frame = new Construction_ihm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Construction_ihm() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 248, 220));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblConstruction = new JLabel("Construction");
		lblConstruction.setFont(new Font("Century Gothic", Font.BOLD, 13));
		lblConstruction.setBounds(191, 25, 101, 17);
		contentPane.add(lblConstruction);
		
		JLabel lblNomDeLa = new JLabel("Nom de la pièce : ");
		lblNomDeLa.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblNomDeLa.setBounds(21, 64, 101, 16);
		contentPane.add(lblNomDeLa);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(134, 59, 130, 26);
		contentPane.add(textField);
		
		JLabel lblTypeDeCapteur = new JLabel("Type de capteur :");
		lblTypeDeCapteur.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblTypeDeCapteur.setBounds(21, 92, 119, 29);
		contentPane.add(lblTypeDeCapteur);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		comboBox.setBounds(21, 123, 100, 27);
		contentPane.add(comboBox);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		comboBox_1.setBounds(169, 123, 100, 27);
		contentPane.add(comboBox_1);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		comboBox_2.setBounds(302, 123, 100, 27);
		contentPane.add(comboBox_2);
		
		JLabel lblMarque = new JLabel("Marque : ");
		lblMarque.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblMarque.setBounds(190, 98, 59, 16);
		contentPane.add(lblMarque);
		
		JLabel lblNombre = new JLabel("Nombre :");
		lblNombre.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblNombre.setBounds(335, 98, 67, 16);
		contentPane.add(lblNombre);
		
		JButton button = new JButton("Simuler");
		button.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		button.setBounds(31, 191, 87, 68);
		contentPane.add(button);
		
		JButton button_1 = new JButton("Ajouter");
		button_1.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		button_1.setBounds(342, 191, 87, 68);
		contentPane.add(button_1);
		
		JLabel lblCotDachat = new JLabel("Coût d'achat :");
		lblCotDachat.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblCotDachat.setBounds(179, 169, 79, 16);
		contentPane.add(lblCotDachat);
		
		JLabel lblCotDeMaintenance = new JLabel("Coût de maintenance sur 10 ans :");
		lblCotDeMaintenance.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblCotDeMaintenance.setBounds(133, 215, 189, 16);
		contentPane.add(lblCotDeMaintenance);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(162, 190, 130, 26);
		contentPane.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(162, 233, 130, 26);
		contentPane.add(textField_2);
	}

}
