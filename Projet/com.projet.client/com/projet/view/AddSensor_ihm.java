package com.projet.view;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.database.ConnectionPool;
import com.database.SensorDAOImplement;
import com.projet.model.Sensor;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.Color;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class AddSensor_ihm extends JFrame implements ActionListener{


	/**
	 *
	 * @author Sofia
	 */
	/*private JMenuBar menu;
	private JMenu ajout = new JMenu("Ajouter un capteur");
	private JMenu search = new JMenu("Rechercher un capteur");
	private JMenu prevision = new JMenu("Prévision des coûts");*/


	private JPanel contentPane;
	private JTextField simul;
	public JButton addCapteur;
	private JTextField number;
	private JComboBox jmarque;
	private JComboBox jloc;
	private JComboBox jtypeCapt;
	private JButton btnSimuler;
	private JTextField done;
	private double price;
	private int garanty;
	private int capacity;
	private double anual_maintenance_price;
	private Sensor sensor;
	private ResultSet rs;
	private PreparedStatement ps;
	private String st;


	public AddSensor_ihm() {

		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 248, 220));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);


		JLabel lblAjouterUnCapteur = new JLabel("Ajouter un capteur");
		lblAjouterUnCapteur.setFont(new Font("Century Gothic", Font.BOLD, 13));
		lblAjouterUnCapteur.setBounds(167, 6, 131, 16);
		contentPane.add(lblAjouterUnCapteur);

		JLabel type_capt = new JLabel("Type de capteur");
		type_capt.setFont(new Font("Century Gothic", Font.BOLD, 11));
		type_capt.setBounds(24, 51, 100, 16);
		contentPane.add(type_capt);

		JLabel lblMarque = new JLabel("Marque");
		lblMarque.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblMarque.setBounds(206, 51, 61, 16);
		contentPane.add(lblMarque);

		jloc = new JComboBox();
		jloc.setModel(new DefaultComboBoxModel(new String[] {"couloir", "toilettes", "salle animation collective ", "salle activite", "salle de soins", "accueil", "centre operationnel"}));
		jloc.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		jloc.setBounds(318, 71, 100, 27);
		contentPane.add(jloc);

		JLabel lblLocalisation = new JLabel("Localisation");
		lblLocalisation.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblLocalisation.setBounds(338, 51, 80, 16);
		contentPane.add(lblLocalisation);

		jmarque = new JComboBox();
		jmarque.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		jmarque.setModel(new DefaultComboBoxModel(new String[] {"NewOne", "Insafe", "Steihel", "Brennenstuhl", "Iduino", "Microbot", "PyroControle", "Adafruit", "ArtMotion", "Merkur", "Powermaster", "Silamp"}));
		jmarque.setBounds(179, 71, 100, 27);
		contentPane.add(jmarque);

		addCapteur = new JButton("Ajouter");
		addCapteur.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		addCapteur.setBounds(318, 180, 87, 68);
		contentPane.add(addCapteur);

		jtypeCapt = new JComboBox();
		jtypeCapt.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		jtypeCapt.setModel(new DefaultComboBoxModel(new String[] {"detecteur de fumee", "detecteur de mouvement", "capteur de lumiere", "capteur de temperature", "ouverture des portes", "compteur"}));
		jtypeCapt.setBounds(20, 70, 104, 27);
		contentPane.add(jtypeCapt);

		JLabel lblCotDajout = new JLabel("Coût d'ajout :");
		lblCotDajout.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblCotDajout.setBounds(188, 180, 79, 16);
		contentPane.add(lblCotDajout);

		simul = new JTextField();
		simul.setBounds(168, 205, 130, 26);
		contentPane.add(simul);
		simul.setColumns(10);

		btnSimuler = new JButton("Simuler");
		btnSimuler.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		btnSimuler.setBounds(62, 180, 87, 68);
		contentPane.add(btnSimuler);

		JLabel lblNumber = new JLabel("Nombre");
		lblNumber.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblNumber.setBounds(155, 115, 48, 16);
		contentPane.add(lblNumber);

		number = new JTextField();
		number.setColumns(10);
		number.setBounds(203, 110, 42, 26);
		contentPane.add(number);

		done = new JTextField();
		done.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		done.setBounds(86, 256, 293, 16);
		contentPane.add(done);
		done.setColumns(10);

		btnSimuler.addActionListener(this);
		addCapteur.addActionListener(this);

	}


	public void actionPerformed(ActionEvent e) {
		String type;
		String brand;
		String location; 


		brand = jmarque.getSelectedItem().toString();
		location = jloc.getSelectedItem().toString();
		String typeCapt = jtypeCapt.getSelectedItem().toString();
		Connection co;
		ConnectionPool cp = new ConnectionPool();
		co = cp.getConnectionPool();
		PreparedStatement ps = null;
		SensorDAOImplement sdi = new SensorDAOImplement(co);

		/*switch(marque) {
		case "NewOne" :
			brand = "NewOne";
			break;
		case "Insafe" :
			brand = "Insafe";
			break;
		case "Steihel" :
			brand="Steihel";
			break;
		case "Brennenstuhl" :
			brand = "Brennenstuhl";
			break;
		case "Iduino" : 
			brand = "Iduino";
			break;
		case "PyroControle" : 
			brand = "Pyrocontrole";
			break;
		case "Adafruit" :
			brand = "Adafruit";
			break;
		case "ArtMotion" :
			brand = "ArtMotion";
			break;
		case "Merkur" :
			brand = "Merkur";
			break;
		case "Powermaster" :
			brand = "Powermaster";
			break;
		case "Silamp" :
			brand = "Silamp";
			break;
		default :
			brand="NewOne";
			break;
		}

		switch(loc) {
		case "couloir" :
			localisation = "couloir";
			break;
		case "salle animation collective" :
			localisation = "salle animation collective";
			break;
		case "salle activite" :
			localisation="salle activite";
			break;
		case "salle de soins" :
			localisation = "salle de soins";
			break;
		case "accueil" : 
			localisation = "accueil ";
			break;
		case "centre operationnel" : 
			localisation = "centre operationnel";
			break;
		case "toilettes" :
			localisation = "toilettes";
			break;
		default : 
			localisation="couloir";
			break;
		}*/

		switch(typeCapt) {
		case "detecteur de fumee" :
			type = "detecteur_fumee";
			break;
		case "detecteur de mouvement" :
			type = "detecteur_mouvement";
			break;
		case "capteur de lumiere" :
			type ="capteur_lumiere";
			break;
		case "capteur de temperature" :
			type = "capteur_temperature";
			break;
		case "ouverture des portes" : 
			type = "ouverture_porte ";
			break;
		case "compteur" : 
			type = "compteur";
			break;
		default :
			type="detecteur_fumee";
			break;
		}


		/*String st_p = "select prix from capteurs where nom_capteur='"+type+"' and marque='"+brand+"'";
		String st_c = "select portee from capteurs where nom_capteur="+type;
		String st_g = "select duree_garantie from capteurs where nom_capteur="+type+" and marque="+brand;
		String st_m = "select cout_maintenance_annee from capteurs where nom_capteur="+type+" and marque ="+brand;*/

		this.st = "select * from capteurs where nom_capteur='"+type+"' and marque='"+brand+"'";
		try {
			this.ps = co.prepareStatement(this.st);
			this.rs = this.ps.executeQuery();
			while(this.rs.next()) {
				this.price = this.rs.getDouble("prix");
				this.capacity = this.rs.getInt("portee"); 
				this.garanty = this.rs.getInt("duree_garantie");
				this.anual_maintenance_price = this.rs.getDouble("cout_maintenance_annee");
				this.sensor = new Sensor(type, brand, this.price, this.capacity, this.garanty, location, this.anual_maintenance_price);
			}
		} 

		catch (Exception e1) {
		}

		if(e.getSource()==addCapteur)
		{

			try {
				sdi.insert(sensor);
				done.setText("Capteur ajouté");
			}
			catch (Exception e1) {
			}
		}
		else if (e.getSource()==btnSimuler) {
			String st_nb = number.getText();
			int nb = Integer.parseInt(st_nb.trim());

			try {
				Double simulation = (this.price+this.anual_maintenance_price)*nb;
				simul.setText("Total = "+simulation+"€");
				cp.ConnectionToPool(co);
			} 
			catch(Exception e2) {
			}

		}

	}
	public static void main(String[] args) {
		AddSensor_ihm test = new AddSensor_ihm();
	}
}
