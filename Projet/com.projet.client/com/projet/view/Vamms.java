package com.projet.view;

import java.awt.CardLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Vamms implements ActionListener {

	private JFrame frame;
	private JButton btnConfigurer;
	private JPanel panel;
	private CardLayout cards;
	private JPanel cardPanel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Vamms window = new Vamms();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Vamms() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		cardPanel = new JPanel();
		frame = new JFrame();
		frame.setBounds(100, 100, 696, 490);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		cards = new CardLayout(0,0);
		cardPanel.add(new ConfigView(), "1");

		panel = new JPanel();
		panel.setBounds(6, 6, 684, 375);
		panel.setLayout(cards);
		frame.getContentPane().add(panel);
		panel.add(cardPanel);
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(23, 393, 653, 69);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);

		JButton btnMapper = new JButton("Mapper");
		btnMapper.setBounds(16, 0, 110, 63);
		panel_1.add(btnMapper);

		JButton btnFinance = new JButton("Finance");
		btnFinance.setBounds(526, 0, 110, 63);
		panel_1.add(btnFinance);

		JButton btnMonitorer = new JButton("Monitorer");
		btnMonitorer.setBounds(260, 0, 110, 63);
		panel_1.add(btnMonitorer);

		JButton btnIndicateurs = new JButton("Indicateurs");
		btnIndicateurs.setBounds(390, 0, 113, 63);
		panel_1.add(btnIndicateurs);

		btnConfigurer = new JButton("Configurer");
		btnConfigurer.addActionListener(this);
		btnConfigurer.setBounds(138, 0, 110, 63);
		panel_1.add(btnConfigurer);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnConfigurer)
		{
			cards.show(cardPanel, "1");
			cardPanel.repaint();
			cardPanel.revalidate();
		}
	}
}
