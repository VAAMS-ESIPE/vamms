package com.projet.view;



import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JSpinner;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JSlider;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JSeparator;

public class ConfigView extends JPanel {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	

	/**
	 * Create the frame.
	 */
	public ConfigView() {
		
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ID capteur :");
		lblNewLabel.setBounds(10, 11, 86, 14);
		contentPane.add(lblNewLabel);
		
		JList list = new JList();
		list.setBounds(137, 115, -20, -22);
		contentPane.add(list);
		
		JSlider slider = new JSlider();
		slider.setToolTipText("");
		slider.setBounds(50, 134, 200, 26);
		contentPane.add(slider);
		
		JLabel lblNewLabel_1 = new JLabel("Seuil de sensibilit\u00E9 du capteur :");
		lblNewLabel_1.setBounds(10, 120, 157, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Adresse IP :");
		lblNewLabel_2.setBounds(163, 11, 64, 14);
		contentPane.add(lblNewLabel_2);
		
		textField = new JTextField();
		textField.setBounds(225, 8, 97, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Param\u00E8tres :");
		lblNewLabel_3.setBounds(10, 36, 64, 14);
		contentPane.add(lblNewLabel_3);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Detecte Monoxyde");
		chckbxNewCheckBox.setBounds(10, 57, 125, 23);
		contentPane.add(chckbxNewCheckBox);
		
		JCheckBox chckbxNewCheckBox_2 = new JCheckBox("Priv\u00E9e");
		chckbxNewCheckBox_2.setBounds(10, 83, 97, 23);
		contentPane.add(chckbxNewCheckBox_2);
		
		JCheckBox chckbxNewCheckBox_3 = new JCheckBox("Signal lumineux");
		chckbxNewCheckBox_3.setBounds(120, 83, 107, 23);
		contentPane.add(chckbxNewCheckBox_3);
		
		JLabel lblNewLabel_4 = new JLabel("Type :");
		lblNewLabel_4.setBounds(10, 171, 46, 14);
		contentPane.add(lblNewLabel_4);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"fum\u00E9e", "mouvement", "temp\u00E9rature", "lumi\u00E8re"}));
		comboBox_1.setBounds(50, 171, 67, 20);
		contentPane.add(comboBox_1);
		
		JLabel lblNewLabel_5 = new JLabel("Etat :");
		lblNewLabel_5.setBounds(137, 171, 46, 14);
		contentPane.add(lblNewLabel_5);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"Par d\u00E9faut", "Actif", "Inactif", "En panne", "En pause"}));
		comboBox_2.setBounds(173, 171, 77, 20);
		contentPane.add(comboBox_2);
		
		JButton btnNewButton = new JButton("Valider");
		btnNewButton.setBounds(299, 212, 89, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel_6 = new JLabel("Masque de sous r\u00E9seau :");
		lblNewLabel_6.setBounds(163, 36, 125, 14);
		contentPane.add(lblNewLabel_6);
		
		textField_1 = new JTextField();
		textField_1.setBounds(284, 33, 97, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblGateway = new JLabel("Gateway :");
		lblGateway.setBounds(163, 61, 54, 14);
		contentPane.add(lblGateway);
		
		textField_2 = new JTextField();
		textField_2.setBounds(214, 58, 97, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(71, 8, 77, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
	}
}
