package com.projet.view;


import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.database.ConnectionPool;
import com.database.SensorDAOImplement;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.Color;

import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Color;

public class Maintenance_ihm extends JFrame implements ActionListener{

	private JPanel contentPane;
	private JTextField textField;
	private ResultSet rs;
	private PreparedStatement ps;
	private String st;
	private JButton ok;
	private JRadioButton oneYear;
	private JRadioButton fiveYears;
	private JRadioButton tenYears;
	private int garanty;
	private double anual_maintenance_price;
	private JComboBox type_Capt;
	private JComboBox loc;
	private String brand;
	private JTextField cout;
	private JTextField jmarque;


	public Maintenance_ihm() {
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 248, 220));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblMaintenance = new JLabel("Maintenance");
		lblMaintenance.setBounds(179, 6, 101, 17);
		lblMaintenance.setFont(new Font("Century Gothic", Font.BOLD, 13));
		contentPane.add(lblMaintenance);

		JLabel jtype_capt = new JLabel("Type de capteur");
		jtype_capt.setFont(new Font("Century Gothic", Font.BOLD, 11));
		jtype_capt.setBounds(234, 60, 100, 16);
		contentPane.add(jtype_capt);

		type_Capt = new JComboBox();
		type_Capt.setModel(new DefaultComboBoxModel(new String[] {"detecteur de fumee", "detecteur de mouvement", "capteur de lumiere", "capteur de temperature", "ouverture des portes", "compteur"}));
		type_Capt.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		type_Capt.setBounds(230, 88, 104, 27);
		contentPane.add(type_Capt);

		JLabel jloc = new JLabel("Localisation");
		jloc.setFont(new Font("Century Gothic", Font.BOLD, 11));
		jloc.setBounds(130, 60, 80, 16);
		contentPane.add(jloc);

		loc = new JComboBox();
		loc.setModel(new DefaultComboBoxModel(new String[] {"couloir", "toilettes", "salle animation collective", "salle activite", "salle de soins", "accueil", "centre operationnel"}));
		loc.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		loc.setBounds(118, 88, 100, 27);
		contentPane.add(loc);

		JLabel lblVoirLePrix = new JLabel("Voir le prix de la maintenance sur :");
		lblVoirLePrix.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblVoirLePrix.setBounds(130, 127, 188, 27);
		contentPane.add(lblVoirLePrix);

		oneYear = new JRadioButton("1 an");
		oneYear.setBounds(98, 155, 59, 23);
		contentPane.add(oneYear);

		fiveYears = new JRadioButton("5 ans");
		fiveYears.setBounds(189, 155, 66, 23);
		contentPane.add(fiveYears);

		tenYears = new JRadioButton("10 ans");
		tenYears.setBounds(267, 155, 141, 23);
		contentPane.add(tenYears);

		ok = new JButton("Ok");
		ok.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		ok.setBounds(199, 190, 50, 27);
		contentPane.add(ok);

		cout = new JTextField();
		cout.setColumns(10);
		cout.setBounds(88, 246, 130, 26);
		contentPane.add(cout);

		JLabel lblCout = new JLabel("Coût :");
		lblCout.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblCout.setBounds(44, 251, 32, 16);
		contentPane.add(lblCout);

		JLabel marque = new JLabel("Marque :");
		marque.setFont(new Font("Century Gothic", Font.BOLD, 11));
		marque.setBounds(242, 251, 61, 16);
		contentPane.add(marque);

		jmarque = new JTextField();
		jmarque.setColumns(10);
		jmarque.setBounds(297, 246, 130, 26);
		contentPane.add(jmarque);

		ok.addActionListener(this);
		oneYear.addActionListener(this);
		fiveYears.addActionListener(this);
		tenYears.addActionListener(this);



	}


	public void actionPerformed(ActionEvent e) {
		String type;
		String location; 


		location = loc.getSelectedItem().toString();
		Connection co;
		ConnectionPool cp = new ConnectionPool();
		co = cp.getConnectionPool();
		PreparedStatement ps = null;
		SensorDAOImplement sdi = new SensorDAOImplement(co);

		switch(type_Capt.getSelectedItem().toString()) {
		case "detecteur de fumee" :
			type = "detecteur_fumee";
			break;
		case "detecteur de mouvement" :
			type = "detecteur_mouvement";
			break;
		case "capteur de lumiere" :
			type ="capteur_lumiere";
			break;
		case "capteur de temperature" :
			type = "capteur_temperature";
			break;
		case "ouverture des portes" : 
			type = "ouverture_porte ";
			break;
		case "compteur" : 
			type = "compteur";
			break;
		default :
			type="detecteur_fumee";
			break;
		}

		this.st = "select * from capteurs where nom_capteur='"+type+"' and localisation='"+location+"'";
		try {
			this.ps = co.prepareStatement(this.st);
			this.rs = this.ps.executeQuery();
			while(this.rs.next()) {

				this.anual_maintenance_price = this.rs.getDouble("cout_maintenance_annee");
				this.brand = this.rs.getString("marque");
			}
		} 

		catch (Exception e1) {
		}

		if (e.getSource()==ok) {
			if (oneYear.isSelected()) {
				cout.setText(""+this.anual_maintenance_price);
				jmarque.setText(""+this.brand);
				//cp.ConnectionToPool(co);
			}
			else if (fiveYears.isSelected()) {
				cout.setText(""+(this.anual_maintenance_price*5));
				jmarque.setText(""+this.brand);
				//cp.ConnectionToPool(co);
			}
			
			else if (tenYears.isSelected()) {
				cout.setText(""+(this.anual_maintenance_price*10));
				jmarque.setText(""+this.brand);
				//cp.ConnectionToPool(co);
			}
			cp.ConnectionToPool(co);

		}

		cp.closeAllConnection();

	}


}
